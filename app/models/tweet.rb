# == Schema Information
#
# Table name: tweets
#
#  id         :integer          not null, primary key
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Tweet < ApplicationRecord
  belongs_to :user

  validates_length_of :body, maximum: 180, message: 'Excede the limit of characters'

#  scope :user_tweets, -> { where(user_id: user_id)}

  private

#  def unique_tweet
#    last_tweet = user_tweets.last
#    if last_tweet.created_at < Time.zone.now
#       self.body == last_tweet 
#    end
  #  
#  end
end
