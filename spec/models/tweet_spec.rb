require 'rails_helper'

RSpec.describe Tweet, type: :model do
 
  subject { FactoruBot(:tweet)}

  describe 'Valiation' do
    it do
      should validate_length_of(:body)
        is_at_most(180)
    end
  end
end
